package TruthTable;

import javax.swing.plaf.nimbus.State;

public class TruthTable {
    private int itsRows;
    private int itsCols;
    private String itsExpression;
    private Statement[] itsStatements;

    TruthTable(String expression) throws Exception {
        itsExpression = expression;
        itsStatements = buildStatements(expression);
    }

    private Statement[] buildStatements(String expression) throws Exception {
        int numStatements = getNumStatements(expression);
        int parenthesisLevel = 0;
        int curStatement = 0;
        String[] stringStatements = new String[numStatements];
        for (int i = 0; i < numStatements; i++) {
            stringStatements[i] = "";
        }
        int[] statementPriorities = new int[numStatements];

        for (int i = 0; i < expression.length(); i++) {
            switch (expression.charAt(i)) {
                case '(':
                    parenthesisLevel++;
                    break;
                case ')':
                    parenthesisLevel--;
                    if (parenthesisLevel < 0) {
                        throw new Exception("Invalid Parenthesis Mapping (Close)!");
                    }
                    break;
                case '=':
                case '!':
                case '>':
                case '<':
                case '≥':
                case '≤':
                    stringStatements[curStatement] += expression.charAt(i);
                    statementPriorities[curStatement] = parenthesisLevel;
                    break;
                case '&':
                case '|':
                    curStatement++;
                    stringStatements[curStatement] += expression.charAt(i);
                    statementPriorities[curStatement] = parenthesisLevel;
                    curStatement++;
                    break;
                case '~':
                    stringStatements[curStatement] += expression.charAt(i);
                    statementPriorities[curStatement] = parenthesisLevel;
                    curStatement++;
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    stringStatements[curStatement] += expression.charAt(i);
                    break;
                default:
                    throw new Exception("Invalid Expression!");
            }
        }

        if (parenthesisLevel > 0) {
            throw new Exception(("Invalid Parenthesis Mapping (Open)!"));
        }

        System.out.println("EXPRESSION:\n" + expression + "\n");

        System.out.println("STATEMENTS:");

        for (String i : stringStatements) {
            System.out.println(i);
        }

        System.out.println("\nMAPPING:");

        for (int i : statementPriorities) {
            System.out.print(i + " ");
        }

        Statement[] ret = new Statement[numStatements];

        int prev = statementPriorities[0];
        int highest = prev;
        int lowest = prev;
        for (int i = 1; i < numStatements; i++) {
            if (statementPriorities[i] > highest) {
                highest = statementPriorities[i];
            }
            if (statementPriorities[i] < lowest) {
                if (statementPriorities[i] > -1) {
                    lowest = statementPriorities[i];
                } else {
                    throw (new Exception(("Default (-1) or Invalid (<-1) priority level found, cannot evaluate statement!")));
                }
            }
            if (statementPriorities[i] < prev) {
                ret[i - 1] = new BasicStatement(getBOp1(stringStatements[i - 1]), getBOp(stringStatements[i - 1]), getBOp2(stringStatements[i - 1]), statementPriorities[i - 1]);
            }
            prev = statementPriorities[i];
        }
        ret[numStatements - 1] = new BasicStatement(getBOp1(stringStatements[numStatements - 1]), getBOp(stringStatements[numStatements - 1]), getBOp2(stringStatements[numStatements - 1]), statementPriorities[numStatements - 1]);

        for (int i = highest - 1; i >= lowest; i--) { //For every possible priority in order from highest to lowest...
            int previous = 0;
            for (int j = 0; j < statementPriorities.length; j++) { //For every priority in the priorities list.
                if (statementPriorities[j] == i + 1) {
                    previous = j;
                }
                if (statementPriorities[j] == i) { //If the current priority in the priorities list is next to be selected according to the heirarchy of priotities.
                    if (stringStatements[j].length() == 1) { //If it is a complex statement...
                        if (stringStatements[j].equals("~")) { //If the current statement with that priority is a "not".
                            ret[j] = new ComplexStatement(ret[j + 1].getItsValue(), stringStatements[j].charAt(0), statementPriorities[j]);
                        } else {
                            ret[j] = new ComplexStatement(ret[previous].getItsValue(), stringStatements[j].charAt(0), ret[j + 1].getItsValue(), statementPriorities[j]);
                        }
                    } else { //If it is a basic statement...
                        ret[j] = new BasicStatement(getBOp1(stringStatements[j]), getBOp(stringStatements[j]), getBOp2(stringStatements[j]), statementPriorities[j]);
                    }
                }
            }
        }


        System.out.println("\n");

        System.out.println("LOGIC MAP:");
        for (int i = highest; i >= lowest; i--) {
            System.out.print(i + "   ");
            for (int j = 0; j < highest*3-i*3; j++) {
                System.out.print("-");
            }
            for (int j = 0; j < statementPriorities.length; j++) {
                if (statementPriorities[j] == i) {
                    System.out.print(ret[j]);
                    for (int k = 0; k < 6*i; k++) {
                        System.out.print("-");
                    }
                }
            }
            System.out.println("");
        }

        System.out.println("\n\nVERDICT:");
        for (int i = 0; i < statementPriorities.length; i++) {
            if (statementPriorities[i] == lowest) {
                System.out.println("=== " + ret[i].getItsValue() + " ==="); //Output the verdict.
            }
        }

//        for (int i = 0; i < statementPriorities.length; i++) {
//            System.out.println(ret[i].getItsPriority() + ", " + ret[i].getItsOperator() + ", " + ret[i].getItsValue());
//        }

        return ret;
    }

    public int getNumStatements(String expression) {
        int ret = 0;
        char[] operators = new char[]{'=', '!', '>', '<', '≥', '≤', '&', '|', '~'};

        for (int i = 0; i < expression.length(); i++) {
            for (char operator : operators) {
                if (expression.charAt(i) == operator) {
                    ret++;
                }
            }
        }

        return ret;
    }

    private int getBOp1(String expr) {
        int ret = -995; //random value for flagging
        String temp = "";
        char[] operators = new char[]{'=', '!', '>', '<', '≥', '≤'};

        for (int i = 0; i < expr.length(); i++) {
            for (int j = 0; j < operators.length; j++) {
                if (expr.charAt(i) == operators[j]) {
                    return Integer.parseInt(temp);
                }
            }
            temp += expr.charAt(i);
        }

        return ret;
    }

    private char getBOp(String expr) {
        char ret = ' ';
        char[] operators = new char[]{'=', '!', '>', '<', '≥', '≤'};

        for (int i = 0; i < expr.length(); i++) {
            for (int j = 0; j < operators.length; j++) {
                if (expr.charAt(i) == operators[j]) {
                    return operators[j];
                }
            }
        }
        return ret;
    }

    private int getBOp2(String expr) {
        int ret = -995; //random value for flagging
        String temp = "";
        char[] operators = new char[]{'=', '!', '>', '<', '≥', '≤'};
        boolean searching = false;

        for (int i = 0; i < expr.length(); i++) {
            if (searching) {
                temp += expr.charAt(i);
            }
            for (int j = 0; j < operators.length; j++) {
                if (expr.charAt(i) == operators[j]) {
                    searching = true;
                }
            }
        }

        ret = Integer.parseInt(temp);
        return ret;
    }
}
