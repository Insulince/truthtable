package TruthTable;

public class BasicStatement extends Statement {
    private int itsOperand1;
    private int itsOperand2;

    BasicStatement(int operand1, char operator, int operand2) throws Exception {
        this(operand1, operator, operand2, -1);
    }

    BasicStatement(int operand1, char operator, int operand2, int priority) throws Exception {
        itsOperand1 = operand1;
        itsOperator = operator;
        itsOperand2 = operand2;
        itsPriority = priority;
        itsValue = calculateItsValue();
        itsStatement = "" + operand1 + operator + operand2;
    }

    BasicStatement(boolean value) {
        this(value, -1);
    }

    BasicStatement(boolean value, int priority) {
        itsValue = value;
        itsPriority = priority;
        itsStatement = "" + value;
    }

    public int getItsOperand1() {
        return itsOperand1;
    }

    public int getItsOperand2() {
        return itsOperand2;
    }

    public String toString() {
        return "" + itsOperand1 + itsOperator + itsOperand2;
    }

    private boolean calculateItsValue() throws Exception {
        switch (itsOperator) {
            case '=':
                return itsOperand1 == itsOperand2;
            case '!':
                return itsOperand1 != itsOperand2;
            case '>':
                return itsOperand1 > itsOperand2;
            case '<':
                return itsOperand1 < itsOperand2;
            case '≥':
                return itsOperand1 >= itsOperand2;
            case '≤':
                return itsOperand1 <= itsOperand2;
            default:
                throw (new Exception("Unrecognized Operator!"));
        }
    }
}
