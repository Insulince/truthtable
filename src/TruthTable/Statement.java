package TruthTable;

public class Statement {
    public char itsOperator;
    public boolean itsValue;
    public int itsPriority;
    public String itsStatement;
//    public Statement itsStatement;

//    Statement(int operand1, char operator, int operand2, int priority) throws Exception {
//        itsStatement = new BasicStatement(operand1, operator, operand2, priority);
//    }
//
//    Statement(boolean operand1, char operator, int priority) throws Exception {
//        itsStatement = new ComplexStatement(operand1, operator, priority);
//    }
//
//    Statement(boolean operand1, char operator, boolean operand2, int priority) throws Exception {
//        itsStatement = new ComplexStatement(operand1, operator, operand2, priority);
//    }
//
//    Statement(boolean value) {
//        itsStatement = new BasicStatement(value);
//    }
//
//    public Statement() {
//
//    }
//
//    public Statement getItsStatement() {
//        return itsStatement;
//    }

    public boolean getItsValue() {
        return itsValue;
    }

    public char getItsOperator() {
        return itsOperator;
    }

    public int getItsPriority() {
        return itsPriority;
    }
}
