package TruthTable;

public class ComplexStatement extends Statement {
    private boolean itsOperand1;
    private boolean itsOperand2;

    ComplexStatement(boolean operand1, char operator) throws Exception {
        this(operand1, operator, false, -1);
    }

    ComplexStatement(boolean operand1, char operator, boolean operand2) throws Exception {
        this(operand1, operator, operand2, -1);
    }

    ComplexStatement(boolean operand1, char operator, int priority) throws Exception {
        this(operand1, operator, false, priority);
    }

    ComplexStatement(boolean operand1, char operator, boolean operand2, int priority) throws Exception {
        itsOperand1 = operand1;
        itsOperator = operator;
        itsOperand2 = operand2;
        itsPriority = priority;
        itsValue = calculateItsValue();
        if (operator != '~') {
            itsStatement = "" + operand1 + operator + operand2;
        } else {
            itsStatement = "" + operator + operand1;
        }
    }

    public boolean getItsOperand1() {
        return itsOperand1;
    }

    public boolean getItsOperand2() throws Exception {
        if (itsOperator != '~') {
            return itsOperand2;
        } else {
            throw (new Exception("Not operator (~) has no second operand to return!"));
        }
    }

    public String toString() {
        return itsStatement;
    }

    private boolean calculateItsValue() throws Exception {
        switch (itsOperator) {
            case '&':
                return itsOperand1 && itsOperand2;
            case '|':
                return itsOperand1 || itsOperand2;
            case '~':
                if (itsOperand1 == true) {
                    return false;
                } else if (itsOperand1 == false) {
                    return true;
                }
            default:
                throw (new Exception("Unrecognized Operator!"));
        }
    }
}
